[[_TOC_]]

# About Voyager
Voyager Portal is a Demurrage & Voyage Management platform that helps some of the biggest traders and manufacturers in the world reduce and optimize shipping costs.

Picture the work of an Oil manufacturer managing its maritime supply chain. This involves dealing with a network of shipping companies, cargo handling facilities, ports, and customs & regulatory agencies, ensuring all financial and logistical indicators are in order. With Voyager, chartering and demurrage teams can increase operational performance by automating manual tasks, connecting all their counterparties in one platform, and using real-time insights to reduce significant voyage costs such as freight and demurrage.

## About the role

This is a remote position for candidates based in Brazil. We are looking for a Senior Fullstack Software Engineer. The core of the Voyager application’s are NodeJS in the backend and VueJS in the front. Applicants should feel comfortable mentoring other, more-junior developers while still producing considerable code output with excellent quality. Detailed responsibilities include:

* Writing reusable, testable, efficient and KISS (keep it short & simple) principle code
* Design and implementation of low-latency, high-availability, and performance applications
* Integration of data storage solutions (MySQL, Amazon S3)
* Integration with external ERPs and APIs
* Providing help to junior developers while reaching out to senior developers for sense-checks
* Providing recommendations to product team for better future development
* Creating self-contained, reusable, and testable modules and components
* Writing extensive functional and unit tests
* Implementing automated testing platforms and unit tests
* Proficient understanding of code versioning tools, such as Git

## Skills & Qualifications
* **STRONG** English skills, both written and spoken
* We are looking for candidates with minimum of 2yr experience 
* Professional, precise communication skills
* Strong desire to learn, push the envelope, and share knowledge with others

## Bonus
* Knowledge of Node.js
* Knowledge of AdonisJS Framework
* Knowledge of VueJS
* Interest / knowledge in the maritime industry

## Compensation & Benefits
* Competitive salary based on performance and experience
* Salary paid in USD
* Stock options
* For this position the salary range is between 3-4.5K USD

## Other Perks
* Generous annual leave policy
* Global travel annually to our company All Hands (Previous events in USA, Argentina, Panama and Chile)
* Monthly healthcare and technology allowances
* Flexible working arrangement
* International colleagues

# How to Apply

To apply for this position you need to implement the Mars Rover Test and submit it to us. All the details on what's need to be done and how to submit are described on the test.

If you have any other questions about this position please submit them to Sabrina (sabrina@voyagerportal.com)

## Mars Rover in JavaScript

A squad of robotic rovers are to be landed by NASA on a plateau on Mars.

This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.

A rover’s position and location is represented by a combination of x and y co-ordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.

In order to control a rover , NASA sends a simple string of letters. The possible letters are ‘L’, ‘R’ and ‘M’. ‘L’ and ‘R’ makes the rover spin 90 degrees left or right respectively, without moving from its current spot. ‘M’ means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y 1).

### Input

The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0.

The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover’s position, and the second line is a series of instructions telling the rover how to explore the plateau.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y co-ordinates and the rover’s orientation.

Each rover will be finished sequentially, which means that the second rover won’t start to move until the first one has finished moving.

### Output

The output for each rover should be its final co-ordinates and heading.

### Rover Data Example

#### Example 1

Landing Position: 1 2 N \
Instruction: LMLMLMLMM \
Final Position: 1 3 N

#### Example 2
Landing Position: 3 3 E \
Instruction: MRRMMRMRRM \
Final Position: 2 3 S

### What we Expect

- All data input should be done via the user interface.

- Multiple rovers should be supported.

- Backend to be built as an API (Node/Express preferable)

- Frontend for input and data visualization. (VueJS preferrable)

- Whole project to run on Docker

- Clear and detailed documentation on how to run the project

- We will evaluate your code structure, readability, organization, clean code, automated tests and of course if the application works as expected.

- Please add your project to a repo and reply to us via email (sabrina@voyagerportal.com) with the repo URL, you name and phone number.


### Bonus

- Use a database to store the application data. (MySQL/MongoDB preferrable)
